import { useState } from 'react'

export const useInputValue = (initialValue = '') => {
  const [value, setValue] = useState(initialValue)
  const onChange = e => setValue(e.target.value)
  const updateValue = newValue => setValue(newValue)
  return { value, onChange, updateValue }
}
