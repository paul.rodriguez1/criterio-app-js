import { useSelector } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'

export default function PrivateRoute ({ children, ...rest }) {
  const { login: { auth } } = useSelector(state => state)
  return (
    <Route
      {...rest} render={
        ({ location }) =>
          auth
            ? children
            : <Redirect to={{ pathname: '/', state: { from: location } }} />
      }
    />
  )
}
