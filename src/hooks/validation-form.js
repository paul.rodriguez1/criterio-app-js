import { signInServices, signUpServices } from '../services/request'

export const signIn = async ({ email, password, setMsjModal, reset, setFocus }) => {
  setMsjModal({ type: 'loading', msj: 'validandoInfo' })
  const { type, msj } = await signInServices({ email: email, password: password })
  // setTimeout(() => {
  setMsjModal({ type, msj })
  setFocus('email')
  type === 'error' && reset()
  // }, 500)
  return { type, msj }
}

export const signUp = async ({ nombre, email, password, setMsjModal, reset, setFocus }) => {
  setMsjModal({ type: 'loading', msj: 'validandoInfo' })
  const { type, msj } = await signUpServices({ email, password, nombre })
  // setTimeout(() => {
  setMsjModal({ type, msj })
  setFocus('nombre')
  type === 'error' && reset()
  // }, 500)
  return { type, msj }
}
