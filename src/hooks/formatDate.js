const Meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
export const formatDate = ({ value }) => {
  const date = new Date(value)
  return `${date.getDate()} de ${Meses[date.getMonth()]} del ${date.getFullYear()}`
}
