import { css, keyframes } from 'styled-components'

const fadeInMessage = keyframes`
  0%{
      opacity: 0;
      transform: translate(0, 0);
  }
  20%{
      opacity: 0.2;
      transform: translate(0, 20px);
  }
  100%{
      opacity: 1;
      transform: translate(0, 0);
  }
`

export const fadeInMessageAnimation = ({ time = '.3s', type = 'ease', operation = '' } = {}) =>
  css`
    animation: ${time} ${fadeInMessage} ease forwards;
  `
