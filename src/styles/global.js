import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  :root {
    --FontSize: 14px;
    --FontFamily: 'Lato', sans-serif;
    /* Colors */
    --ColorBgApp: white;
    --ColorFont: #4A4A4A;
    --ColorAzul: #384AC3;
    --ColorMorado: #780793;
    --ColorBorder: #C6C6C6;
    --ColorLiHover: #efefef;
    --ColorVerde: #1b5d10;
    --ColorInput: #F5F5F5;
    --ColorError: #e00404;
    --ColorBackgroundInput: #780793;
    --LinearGradienteMain: linear-gradient(90deg, rgba(56,74,195,1) 0%, rgba(120,7,147,1) 100%, rgba(9,9,121,1) 100%);
    --LinearGradienteMainInv: linear-gradient(-90deg, rgba(56,74,195,1) 0%, rgba(120,7,147,1) 100%, rgba(9,9,121,1) 100%);
    --BackgroundModal: rgba(0, 0, 0, .8);
    /* Shadow */
    --ShadowBox: 0 4px 4px rgba(0,0,0,.25);
    --ShadowInput: 2px 2px 2px rgb(0 0 0 / 25%);
    /* Padding */
    --PaddingContainerX: 10%;
    --MaxSizeCard: 310px;
  }
  
  html {
    box-sizing: border-box;
  }

  *, *:before, *:after{
    box-sizing: inherit
  }

  ul{
    padding: 0;
    list-style: none;
    margin: 0;

    li {
      font-family: var(--FontFamily);
      color: var(--ColorFont);
    }
  }

  a {
    text-decoration: none;
    color: var(--ColorFont);
  }

  p {
    margin: 0;
    padding: 0;
  }

  body{
    display: flex;
    min-height: 100vh;
    flex-direction: column;
    font-size: var(--FontSize);
    font-family: var(--FontFamily);
    background-color: #fff;
    background-size: 100%;
    background-attachment: fixed;
    margin: 0;
    padding: 0;
    color: var(--ColorFont);
    line-height: 1.5;
  }

  #root {
    height: 100vh;
  }
`
