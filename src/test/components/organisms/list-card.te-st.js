import { shallow } from 'enzyme'
// import renderer from 'react-test-renderer'
import 'jest-styled-components'
import ListCard from '../../../components/organisms/list-card'

describe('Test to <ListCard />', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<ListCard data={[]} />)
  })

  test('render component', () => {
    expect(wrapper).toMatchSnapshot()
  })

  test('renders content without data', () => {
    const txtWrapper = wrapper.find('h1').text().trim()
    expect(txtWrapper).toBe('No data')
  })
  test('renders content with data', () => {
    const data = [{
      id: 3,
      type: 'satisfaccion-producto',
      title: 'Satisfacción del producto',
      descripcion: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto consectetur nisi illo similique, praesentium laboriosam nulla temporibus dignissimos iste natus nesciunt error a? Obcaecati harum saepe earum asperiores rerum? Reiciendis.',
      typeQuestion: 'selection',
      color: '#1DC71A',
      btnLabel: 'Calificanos!',
      questions: [
        {
          id: 1,
          question: '¿Como te sentiste durante la atención del servicio técnico?',
          input: 'selection'
        },
        {
          id: 2,
          question: '¿Resolviste todas las inquietudes que tenias?',
          input: 'selection'
        },
        {
          id: 3,
          question: 'Cuentanos como podemos mejorar.',
          input: 'textarea'
        }
      ]
    }]
    // valid render success
    wrapper = shallow(<ListCard data={data} />)
    expect(wrapper).toMatchSnapshot()
    // valid props id
    const card = wrapper.props().children[0]
    expect(card.key).toBe(data[0].id.toString())
  })
})
