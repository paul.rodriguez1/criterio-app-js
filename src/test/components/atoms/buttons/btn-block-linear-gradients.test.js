import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import 'jest-styled-components'
import BtnLinearGradient from '../../../../components/atoms/buttons/btn-block-linear-gradient'

describe('Test to <BtnLinearGradient />', () => {
  const label = 'Volver'
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<BtnLinearGradient label={label} />)
  })

  test('render component', () => {
    expect(wrapper).toMatchSnapshot()
  })

  test('renders content', () => {
    const txtWrapper = wrapper.text()
    expect(txtWrapper).toBe(label)
  })

  test('valid styledComponent', () => {
    const wrapper = renderer.create(<BtnLinearGradient />).toJSON()
    expect(wrapper).toHaveStyleRule('opacity', '1')
    expect(wrapper).toHaveStyleRule('opacity', '.2', {
      modifier: ':disabled'
    })
  })
})
