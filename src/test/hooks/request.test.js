import { codesBank, signIn } from '../../services/request'

describe('test hook request async', () => {
  test('Test signIn, without credentials', async () => {
    const response = await signIn({ email: '' })
    expect(response.error).toBe('Las credenciales son obligatorias')
  })

  test('Test signIn, with credentials correct', async () => {
    const response = await signIn({ email: 'paul.rodriguez@pragma.com.co', password: '123456' })
    console.log(response)
    expect(response.success).toBe(codesBank.loginSuccess)
  })

  test('Test signIn, with users not exist', async () => {
    const response = await signIn({ email: 'fail@pragma.com.co', password: '123456' })

    expect(response.error).toBe(codesBank['auth/user-not-found'])
  })
})
