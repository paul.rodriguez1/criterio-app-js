import { renderHook, act } from '@testing-library/react-hooks'
import { useInputValue } from '../../hooks/handleInput'

describe('test hook useInputValue', () => {
  test('return values to defatult', () => {
    const { result } = renderHook(() => useInputValue())

    expect(result.current.value).toBe('')
    expect(typeof result.current.onChange).toBe('function')
    expect(typeof result.current.updateValue).toBe('function')
  })

  test('return value initial', () => {
    const valueInitial = 'Test'
    const { result } = renderHook(() => useInputValue(valueInitial))

    expect(result.current.value).toBe(valueInitial)
  })

  test('return value simulate typing to input', () => {
    const valueInitial = 'Test, Initial'
    const valueTyping = 'Test, typing value'
    const { result } = renderHook(() => useInputValue(valueInitial))
    const { onChange } = result.current
    act(() => {
      onChange({ target: { value: valueTyping } })
    })

    expect(result.current.value).toBe(valueTyping)
  })

  test('return value updated', () => {
    const valueInitial = 'Test, Initial'
    const valueUpdated = 'Test, updated value'
    const { result } = renderHook(() => useInputValue(valueInitial))
    const { updateValue } = result.current
    act(() => {
      updateValue(valueUpdated)
    })

    expect(result.current.value).toBe(valueUpdated)
  })
})
