import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyBbNRM8ds-ZBofaewrZxFEoc_cI69Pc5zM',
  authDomain: 'criteroapp.firebaseapp.com',
  projectId: 'criteroapp',
  storageBucket: 'criteroapp.appspot.com',
  messagingSenderId: '986828232434',
  appId: '1:986828232434:web:412c54ceb3a890e812e26b'
}
// Initialize Firebase
const fb = firebase.initializeApp(firebaseConfig)
export const db = fb.firestore()
export const authFb = fb.auth()
