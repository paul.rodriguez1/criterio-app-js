import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { formatDate } from '../../../hooks/formatDate'
import { getResponses } from '../../../services/request'
import { handleModal } from '../../../redux/actions/utils'
import IconAnswerEmotion from '../../atoms/images/icon-answer-emotion'
import { getImages } from '../../../utils/get-images'
import { getMessageText } from '../../../utils/get-message'

const MeResponseStyled = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax( var(--MaxSizeCard), 1fr ));
  gap: 10px;
  padding: 10px 0;
  height: 100%;
  overflow: auto;

  .card-response {
    padding: 2em ;
    border: 1px solid var(--ColorBorder);
    border-radius: 20px;
    box-shadow: var(--ShadowBox);
    display: flex;
    flex-direction: column;
    gap: 20px;
    
    .title-card {
      position: relative;
      display: flex;
      flex-direction: column;


      &::before{
        content: '';
        width: 100%;
        background-image: var(--LinearGradienteMain);
        position: absolute;
        bottom: -5px;
        height: 2px;
      }
      h2, h3 {
        margin: 0;
      }

      h3 {
        font-size: 1em;
        font-weight: 300;
        span {
          font-weight: 600;
        }
      }
    }

    ul {
      display: flex;
      gap: 20px;
      flex-direction: column;

      .item-question {
        padding: 10px 0;
        border-bottom: 1px solid var(--ColorBorder);
        &:last-child {
          border-bottom: none;
        }

        &.selection {
          display: flex;
          gap: 5px;
          justify-content: space-between;
          align-items: center;
        }
        .response {
          font-size: .95em;
          font-style: italic;
          font-weight: 600;
        }
      }
    }

  }

`

function MeResponse () {
  const { login: { auth } } = useSelector(state => state)
  const [responses, setResponses] = useState([])

  const dispatch = useDispatch()

  useEffect(async () => {
    dispatch(handleModal({ open: true, action: 'loading', label: getMessageText('loadingInfo') }))
    setResponses(await getResponses(auth))
    setTimeout(() => {
      dispatch(handleModal({ open: false, action: '', label: '' }))
    }, 1000)
  }, [])

  return (
    <MeResponseStyled>
      {
        responses.length
          ? responses.map(res => {
            const { id, response, title, date } = res
            return (
              <div className='card-response' key={id}>
                <div className='title-card'>
                  <h2>{title}</h2>
                  <h3>Registrado el <span>{formatDate({ value: date })}</span></h3>
                </div>
                <ul>
                  {
                    response.map((qr, index) => {
                      return (
                        <li className={`item-question ${qr.input}`} key={index}>
                          <p>{qr.question}</p>
                          {qr.input === 'selection' && <IconAnswerEmotion img={getImages(qr.response)} value='Angry' state='Selected' />}
                          {qr.input === 'textarea' && <p className='response'>{qr.response}</p>}
                        </li>
                      )
                    })
                  }
                </ul>
              </div>
            )
          })
          : <h1>No tiene respuestas registradas</h1>
      }
    </MeResponseStyled>
  )
}

export default MeResponse
