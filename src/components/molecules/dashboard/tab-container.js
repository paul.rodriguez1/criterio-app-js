import styled from 'styled-components'
import TabItem from '../../atoms/li/tab-item'

const TabContainerStyled = styled.ul`
  border-bottom: 1px solid var(--ColorBorder);
  display: flex;
  gap: 10px;
  align-items: center;
  justify-content: flex-start;

  li {
    padding: .5rem 1rem;
    margin-bottom: -1px;
    background-color: #fff;
    cursor: pointer;
    transition: .3s all;
    border: 1px solid transparent;
    border-top: 3px solid transparent;
    border-bottom-color: var(--ColorBorder);
    min-height: 45px;

    &.active, &:hover {
      border-top-left-radius: .25rem;
      border-top-right-radius: .25rem;
      border-color: var(--ColorBorder);
      border-top-color: var(--ColorAzul);
      border-bottom-color: white;
      font-weight: 600;
      font-size: 1.05em;
    }
  }
`

function TabContainer ({ options, selected, handleSelectedOption }) {
  return (
    <TabContainerStyled>
      {
        options.map(opt => <TabItem key={opt.id} label={opt.label} id={opt.id} selected={selected} handleSelectedOption={handleSelectedOption} />)
      }
    </TabContainerStyled>
  )
}

export default TabContainer
