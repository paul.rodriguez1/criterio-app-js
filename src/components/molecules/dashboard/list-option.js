import styled from 'styled-components'
import OptionDashboard from '../../atoms/li/li-menu'

const ListOptionStyled = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fit, 250px);
  gap: 20px;
  justify-content: center;
`

function ListOption ({ options }) {
  return <ListOptionStyled>{options?.map(d => <OptionDashboard key={d.id} target={d.target} label={d.label} color={d.color} icon={d.icon} />)}</ListOptionStyled>
}

export default ListOption
