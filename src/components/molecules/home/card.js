// import { useEffect } from 'react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

// import { stateAuth } from '../../../redux/actions/login'
import { handleModal } from '../../../redux/actions/utils'

// import images
import BtnSolid from '../../atoms/buttons/btn-solid'
import { getImages } from '../../../utils/get-images'

// Component
import IconCard from '../../atoms/images/icon-card'
import TitleCard from '../../atoms/titles/title-card'
import ContainerCircule from '../../atoms/ui/container-circule'

const CardHomeStyled = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-radius: 15px;
  box-shadow: var(--ShadowBox);
  border: 1px solid var(--ColorBorder);
  padding: 15px 10px 15px 20px;
  transition: all .3s;
  width: 100%;
  max-width: var(--MaxSizeCard);

  :hover {
    background-color: #f5f5f5;
    border-color: ${({ color }) => color};

    img{
     transform: scale(1.2);
    }
  }

  .card--info {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    gap: 10px;
  }
`

function CardHome ({ color, btnLabel, type, title, id }) {
  const history = useHistory()

  const dispatch = useDispatch()
  const auth = useSelector(({ login }) => login.auth)

  const handleClick = () => {
    const url = `/encuesta/${id}`

    auth
      ? history.push(url)
      : dispatch(handleModal({ open: true, action: 'login', urltmp: url }))
  }

  return (
    <CardHomeStyled color={color}>
      <ContainerCircule color={color}>
        <IconCard image={getImages(type)} />
      </ContainerCircule>
      <div className='card--info'>
        <TitleCard label={title} size='small' />
        <BtnSolid color={color} label={btnLabel} handleClick={handleClick} />
      </div>
    </CardHomeStyled>
  )
}

export default CardHome
