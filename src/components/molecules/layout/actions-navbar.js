import { useSelector } from 'react-redux'
import BtnIcon from '../../atoms/buttons/btn-footer'
// Img
import Home from '../../../img/footer/home-icon.png'
import User from '../../../img/footer/user-icon.png'
import Dashboard from '../../../img/footer/dashboard-icon.png'

function ActionNav () {
  const { login: { auth } } = useSelector(state => state)

  return (
    <>
      <BtnIcon img={Home} target='/' dataTip='Home' position='top' />
      {auth && <BtnIcon img={Dashboard} target='/dashboard' dataTip='Dashboard' position='top' />}
      <BtnIcon img={User} target='/user' dataTip='Perfil' position='top' />
    </>
  )
}

export default ActionNav
