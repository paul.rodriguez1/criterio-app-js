import Styled from 'styled-components'

// Img
import footerBg from '../../../img/footer-mobile.png'

// Components
import ActionNav from './actions-navbar'

const FooterStyled = Styled.footer`
  background-color: white;
  background-image: url(${({ bgMobile }) => bgMobile});
  background-position: center;
  background-repeat: no-repeat;
  height: 52px;
  min-height: 52px;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  width: 100%;
  padding: 0 var(--PaddingContainerX);
  width: 100%;
  max-width: 100%;

`

const FooterMod = () => {
  return (
    <FooterStyled bgMobile={footerBg}>
      <ActionNav />
    </FooterStyled>

  )
}

export default FooterMod
