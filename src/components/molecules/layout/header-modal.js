import { LogoApp } from '../../atoms/images/logo-app'
import Styled from 'styled-components'

// Img
import headerBg from '../../../img/header-mobile.png'
import BtnCloseModal from '../../atoms/buttons/btn-close-modal'

const HeaderStyled = Styled.header`
  background-color: white;
  background-image: url(${({ bgMobile }) => bgMobile});
  background-position: center;
  background-repeat: no-repeat;
  height: 100px;
  min-height: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  position: relative;
`

const HeaderMod = () => {
  const SizeLogo = {
    width: 137,
    height: 20
  }

  return (
    <HeaderStyled bgMobile={headerBg}>
      <LogoApp size={SizeLogo} />
      <BtnCloseModal />
    </HeaderStyled>

  )
}

export default HeaderMod
