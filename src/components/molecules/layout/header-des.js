import { useHistory } from 'react-router-dom'
import Styled from 'styled-components'

// Img
import headerBg from '../../../img/header-desktop.png'

// Components
import { LogoApp } from '../../atoms/images/logo-app'
import ActionNav from './actions-navbar'

const HeaderStyled = Styled.header`
  background-color: white;
  /* background-image: url(${({ bgMobile }) => bgMobile}); */
  background-position: initial;
  background-repeat: no-repeat;
  height: 205px;
  min-height: 205px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  position: relative;

  .headerBg{
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
  }

  .logoApp {
    position: absolute;
    top: 60px;
    left: 25px;
    z-index: 1;
  }

  nav {
    position: absolute;
    top: 0;
    background-image: var(--LinearGradienteMainInv);
    width: 100%;
    height: 70px;
    z-index: 0;
    padding: 0 var(--PaddingContainerX);
    display: flex;
    align-items: center;
    justify-content: flex-end;
    gap: 20px;
  }
`

const HeaderDesktop = () => {
  const SizeLogo = {
    width: 137,
    height: 20
  }

  const history = useHistory()
  const handleClick = () => {
    history.push('/')
  }

  return (
    <HeaderStyled bgMobile={headerBg}>
      <img className='headerBg' src={headerBg} />
      <LogoApp size={SizeLogo} handleClick={handleClick} />
      <nav>
        <ActionNav />
      </nav>
    </HeaderStyled>

  )
}

export default HeaderDesktop
