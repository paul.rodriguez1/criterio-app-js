import styled from 'styled-components'

const FlexContainerStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  &.flexColum {
    flex-direction: column;
  }

  &.alignItemStart {
    align-items: flex-start;
  }
`

function FlexContainer ({ children, classesElement }) {
  return (
    <FlexContainerStyled className={classesElement}>
      {children}
    </FlexContainerStyled>
  )
}

export default FlexContainer
