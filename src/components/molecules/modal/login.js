/* eslint-disable no-debugger */
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useForm } from 'react-hook-form'
import styled from 'styled-components'
import { signIn } from '../../../hooks/validation-form'

// Component
import BtnLinearGradient from '../../atoms/buttons/btn-block-linear-gradient'
import BtnLinkSimple from '../../atoms/buttons/btn-link-simple'
import TitleModal from '../../atoms/titles/title-modal'
import Header from '../layout/header-modal'
import GroupInput from '../ui/group-input'
import MsjModal from '../../atoms/ui/msj'
import { useHistory } from 'react-router-dom'
import { handleModal } from '../../../redux/actions/utils'

const FormInicioSesionStyled = styled.form`
  width: 350px;
  min-height: 470px;
  background-color: white;
  border-radius: 0 0 20px 20px;
  position: relative;

  section {
    padding: 0 40px 30px;

    .form-inputs {
      display: flex;
      flex-direction: column;
      gap: 15px;
    }
  }
`

function FormInicioSesion () {
  const [msjModal, setMsjModal] = useState({})
  const { register, handleSubmit, reset, setFocus, formState: { errors } } = useForm()
  const history = useHistory()
  const dispatch = useDispatch()

  const { utils: { modal: { urltmp } } } = useSelector(state => state)

  const successProcess = () => {
    dispatch(handleModal({ open: false, action: '', tmpUrl: '' }))
    history.push(history.push(urltmp))
    debugger
  }
  const onSubmit = async data => {
    const response = await signIn({ email: data.email, password: data.password, setMsjModal, reset, setFocus })
    response.type === 'success' && successProcess()
  }

  // set Input
  const emailInput = { ...register('email', { required: 'Debe ingresar su correo electrónico.' }) }

  useEffect(() => {
    setFocus('email')
  }, [setFocus])

  return (
    <FormInicioSesionStyled onSubmit={handleSubmit(onSubmit)}>
      <Header />
      <section>
        <TitleModal label='Iniciar sesión' />
        <div className='form-inputs'>
          <GroupInput
            label='Correo electrónico'
            typeInput='email'
            config={emailInput}
            error={errors.email}
          />
          <GroupInput
            label='Contraseña'
            typeInput='password'
            config={{ ...register('password', { required: 'Debe ingresar su contraseñas.' }) }}
            error={errors.password}
          />
          <BtnLinearGradient label='Iniciar sesión' />
        </div>
        <BtnLinkSimple label='Regístrame' target='register' />
      </section>
      {msjModal && <MsjModal config={msjModal} />}
    </FormInicioSesionStyled>
  )
}

export default FormInicioSesion
