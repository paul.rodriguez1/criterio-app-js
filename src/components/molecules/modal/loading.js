import styled from 'styled-components'
import Spinner from '../../atoms/ui/spinner'

const LoadingStyled = styled.div`
  color: white;
`

function Loading ({ label }) {
  return (
    <LoadingStyled>
      <Spinner type='HashLoader' label={label} color='white' styles='center-column' />
    </LoadingStyled>
  )
}

export default Loading
