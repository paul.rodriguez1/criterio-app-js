import { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'

// redux
import { useDispatch, useSelector } from 'react-redux'
import { handleModal } from '../../../redux/actions/utils'

// Components
import BtnLinearGradient from '../../atoms/buttons/btn-block-linear-gradient'
import BtnLinkSimple from '../../atoms/buttons/btn-link-simple'
import TitleModal from '../../atoms/titles/title-modal'
import Header from '../layout/header-modal'
import GroupInput from '../ui/group-input'
import MsjModal from '../../atoms/ui/msj'
import { signUp } from '../../../hooks/validation-form'

const FormRegistroStyled = styled.form`
  width: 350px;
  min-height: 630px;
  background-color: white;
  border-radius: 0 0 20px 20px;
  position: relative;

  section {
    padding: 0 40px 30px;

    .form-inputs {
      display: flex;
      flex-direction: column;
      gap: 15px;
    }
  }
`

function FormRegistro () {
  const [msjModal, setMsjModal] = useState({})
  const { register, handleSubmit, reset, setFocus, watch, formState: { errors } } = useForm()

  const history = useHistory()
  const dispatch = useDispatch()
  const { utils: { modal: { urltmp } } } = useSelector(state => state)

  const successProcess = () => {
    dispatch(handleModal({ open: false, action: '', tmpUrl: '' }))
    history.push(history.push(urltmp))
  }
  const onSubmit = async data => {
    const { nombre, email, password } = data

    const response = await signUp({ nombre, email, password, setMsjModal, reset, setFocus })
    response.type === 'success' && successProcess()
  }
  useEffect(() => {
    setFocus('nombre')
  }, [setFocus])

  // Paramas valid input
  const validBasicPassword = {
    required: 'Deben ingresar la confirmacion de la contraseña.',
    minLength: {
      value: 6,
      message: 'Su contraseña debe tener minimo 6 caracteres'
    }
  }
  const validPassword = {
    ...validBasicPassword,
    validate: { value: value => value !== watch('passwordConfirm') ? 'Las contraseñas no coinciden' : undefined }

  }
  const validConfirmPassword = {
    ...validBasicPassword,
    validate: { value: value => value !== watch('password') ? 'Las contraseñas no coinciden' : undefined }
  }

  return (
    <FormRegistroStyled onSubmit={handleSubmit(onSubmit)}>
      <Header />
      <section>
        <TitleModal label='Registro' />
        <div className='form-inputs'>
          <GroupInput
            label='Nombre'
            config={{ ...register('nombre', { required: 'Debe ingresar su nombre.' }) }}
            error={errors.nombre}
          />
          <GroupInput
            label='Correo electronico'
            typeInput='email'
            config={{ ...register('email', { required: 'Debe ingresar su correo electrónico.' }) }}
            error={errors.email}
          />
          <GroupInput
            label='Contraseña'
            typeInput='password'
            config={{ ...register('password', { ...validPassword }) }}
            error={errors.password}
          />
          <GroupInput
            label='Confirme su contraseña'
            typeInput='password'
            config={{ ...register('passwordConfirm', { ...validConfirmPassword }) }}
            error={errors.passwordConfirm}
          />
          <BtnLinearGradient label='Registrar' />
        </div>
        <BtnLinkSimple label='Iniciar sesión' target='login' />
      </section>
      {msjModal && <MsjModal config={msjModal} />}
    </FormRegistroStyled>
  )
}

export default FormRegistro
