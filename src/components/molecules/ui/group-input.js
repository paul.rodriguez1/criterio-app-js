import styled from 'styled-components'
import LabelMessageInput from '../../atoms/message/label-message-input'
import InputElement from '../../atoms/ui/input'

const GroupInputStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 5px;
  position: relative;
`

function GroupInput ({ label, typeInput, config, error }) {
  return (
    <GroupInputStyled>
      <span>{label}</span>
      <InputElement type={typeInput} config={config} error={error} />
      <LabelMessageInput error={error} />
    </GroupInputStyled>
  )
}

export default GroupInput
