import styled from 'styled-components'

// Components
import IconCard from '../../atoms/images/icon-card'
import LogoPragma from '../../atoms/images/logo-pragma'
import TitleCard from '../../atoms/titles/title-card'
import ContainerCircule from '../../atoms/ui/container-circule'
import CenterContent from '../layout/container-center'

// Utils
import { getImages } from '../../../utils/get-images'

const DetailsStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 12px;
  border-bottom: 1px solid var(--ColorBorder);
  padding-bottom: 25px;

  .title-details {
    flex-direction: column;

    .h3 {
      font-size: 24px;
    }
  }
`

function HeaderDetails ({ color, type, title }) {
  const SizeLogo = {
    width: 53,
    height: 15
  }

  return (
    <DetailsStyled>
      <ContainerCircule color={color} size='small'>
        <IconCard image={getImages(type)} size='small' />
      </ContainerCircule>
      <CenterContent classesElement='flexColum alignItemStart'>
        <TitleCard label={title} size='large' />
        <LogoPragma size={SizeLogo} />
      </CenterContent>
    </DetailsStyled>
  )
}

export default HeaderDetails
