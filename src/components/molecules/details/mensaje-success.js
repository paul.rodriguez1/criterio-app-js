import ImageSvg from '../../atoms/images/img-svg'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'

// Components
import Paragraph from '../../atoms/ui/paragraph'
import BtnLinearGradient from '../../atoms/buttons/btn-block-linear-gradient'
import LogoPragma from '../../atoms/images/logo-pragma'

const MsjSuccessStyled = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 100%;
  justify-content: center;

  .container-img {
    text-align: right;
    margin-top: 20px;
    width: 100%;
  }
`

function MsjSuccess ({ label }) {
  const history = useHistory()

  const handleClick = () => {
    history.push('/')
  }

  const logoSize = {
    width: 52,
    height: 15
  }
  return (
    <MsjSuccessStyled>
      <ImageSvg img='success' />
      <Paragraph label={label} classElement='msj-success' />
      <BtnLinearGradient label='Volver' handleClick={handleClick} />
      <div className='container-img'>
        <LogoPragma size={logoSize} />
      </div>
    </MsjSuccessStyled>
  )
}

export default MsjSuccess
