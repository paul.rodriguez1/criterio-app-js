import { useEffect } from 'react'
import { useInputValue } from '../../../../hooks/handleInput'
import TextAreaElement from '../../../atoms/ui/textarea'

function ResponseTextarea ({ dataId, handleRegister }) {
  const response = useInputValue('')

  useEffect(() => {
    handleRegister({ id: dataId, value: response.value })
  }, [response.value])

  return <TextAreaElement {...response} />
}

export default ResponseTextarea
