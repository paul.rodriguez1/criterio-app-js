import { useState } from 'react'
import styled from 'styled-components'

// Components
import IconAnswerEmotion from '../../../atoms/images/icon-answer-emotion'
import { getImages } from '../../../../utils/get-images'

const SelectionEmotionStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 25px;
`

function SelectionEmotion ({ dataId, handleRegister }) {
  const [answer, setAnswer] = useState('')

  const SizeImg = {
    width: 40,
    height: 40
  }

  const SelectAnswer = (value) => {
    setAnswer(value)
    handleRegister({ id: dataId, value })
  }

  return (
    <SelectionEmotionStyled>
      <IconAnswerEmotion img={getImages('Angry')} value='Angry' size={SizeImg} state={answer === 'Angry' ? 'Selected' : ''} handleClick={SelectAnswer} />
      <IconAnswerEmotion img={getImages('Bored')} value='Bored' size={SizeImg} state={answer === 'Bored' ? 'Selected' : ''} handleClick={SelectAnswer} />
      <IconAnswerEmotion img={getImages('Happy')} value='Happy' size={SizeImg} state={answer === 'Happy' ? 'Selected' : ''} handleClick={SelectAnswer} />
      <IconAnswerEmotion img={getImages('InLove')} value='InLove' size={SizeImg} state={answer === 'InLove' ? 'Selected' : ''} handleClick={SelectAnswer} />
    </SelectionEmotionStyled>
  )
}

export default SelectionEmotion
