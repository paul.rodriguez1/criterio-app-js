// Components
import HeaderMobile from '../molecules/layout/header-mod'
import HeaderDesktop from '../molecules/layout/header-des'

function HeaderGeneral ({ size }) {
  return (
    <>
      {
      size <= 425
        ? <HeaderMobile />
        : <HeaderDesktop />
    }
    </>
  )
}

export default HeaderGeneral
