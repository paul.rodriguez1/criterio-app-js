import { useLayoutEffect, useState } from 'react'
import styled from 'styled-components'
import FooterMod from '../molecules/layout/footer-mod'

// Components
import HeaderGeneral from '../organisms/header'

const LayoutStyled = styled.div`
  height: 100%;
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: 100%;
  margin: 0 auto;

  section {
    background: white;
    height: 100%;
    padding: 20px var(--PaddingContainerX);
    margin: 0 auto;
    width: 100%;
    overflow-y: auto;
  }
`

function Layout ({ children }) {
  const [size, setSize] = useState(0)

  useLayoutEffect(() => {
    function updateSize () {
      setSize(window.innerWidth)
    }
    window.addEventListener('resize', updateSize)
    updateSize()
    return () => window.removeEventListener('resize', updateSize)
  }, [])

  return (
    <LayoutStyled>
      <HeaderGeneral size={size} />
      <section>
        {children}
      </section>
      {size <= 425 && <FooterMod />}
    </LayoutStyled>
  )
}

export default Layout
