
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import styled from 'styled-components'
import { getOptionDashboard } from '../../services/request'
import { handleModal } from '../../redux/actions/utils'
import ListOption from '../molecules/dashboard/list-option'
import { getMessageText } from '../../utils/get-message'

const MenuDashboardStyled = styled.div`
  width: 100%;
`

function MenuDashboard () {
  const [options, setOptions] = useState([])
  const dispatch = useDispatch()
  const { login: { userLoged } } = useSelector(state => state)
  const { admin } = userLoged

  const filterOptions = data => {
    return data.filter(d => !d.admin)
  }
  useEffect(async () => {
    dispatch(handleModal({ open: true, action: 'loading', label: getMessageText('loadingInfo') }))
    const data = await getOptionDashboard()
    data.length && setOptions(admin ? data : filterOptions(data))
    setTimeout(() => {
      dispatch(handleModal({ open: false, action: '', label: '' }))
    }, 1000)
  }, [])

  return (
    <MenuDashboardStyled>
      <ListOption options={options} />
    </MenuDashboardStyled>
  )
}

export default MenuDashboard
