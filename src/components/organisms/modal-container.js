import styled from 'styled-components'
import { createPortal } from 'react-dom'

// Component
import FormInicioSesion from '../molecules/modal/login'
import FormRegistro from '../molecules/modal/register'
import Loading from '../molecules/modal/loading'

const ModalStyled = styled.div`
  display: ${props => props.open ? 'flex' : 'none'};
  align-items: center;
  justify-content: center;
  background: var(--BackgroundModal);
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  align-items: center;
  opacity: ${props => props.open ? 1 : 0};
  transition: all 3s ease;
  z-index: 3;
`

const Modal = ({ open, action, label }) => {
  return createPortal((
    <ModalStyled open={open}>
      {action === 'login' && <FormInicioSesion />}
      {action === 'register' && <FormRegistro />}
      {action === 'loading' && <Loading label={label} />}
    </ModalStyled>
  ), document.getElementById('Modal'))
}

export default Modal
