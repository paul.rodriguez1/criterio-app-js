import styled from 'styled-components'
import NoData from '../atoms/message/no-data'

// import components
import CardHome from '../molecules/home/card'

const ListCardStyled = styled.ul`
  display: grid;
  gap: 10px;
  justify-content: center;
  grid-template-columns: repeat(auto-fit, var(--MaxSizeCard));
`

function ListCard ({ data }) {
  return (
    <ListCardStyled>
      {
        data.length
          ? data.map(d => <CardHome key={d.id} id={d.id} color={d.color} btnLabel={d.btnLabel} type={d.type} title={d.title} />)
          : <NoData />
      }
    </ListCardStyled>
  )
}

export default ListCard
