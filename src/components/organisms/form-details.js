import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'

// Components
import Paragraph from '../atoms/ui/paragraph'
import HeaderDetails from '../molecules/details/header-details'
import SelectionEmotion from '../molecules/details/answer/SelectionEmotion'
import BtnLinearGradient from '../atoms/buttons/btn-block-linear-gradient'
import ResponseTextarea from '../molecules/details/answer/ResponseTextarea'
import MsjSuccess from '../molecules/details/mensaje-success'

const DetailsFormStyled = styled.form`
  display: flex;
  flex-direction: column;
  gap: 20px;
  height: 100%;
  justify-content: center;
  align-items: center;

  .card-question {
    border: 1px solid var(--ColorBorder);
    padding: 30px 20px;
    border-radius: 14px;
    box-shadow: var(--ShadowBox);
    max-width: 450px;
    width: 100%;

    p {
      margin: 0 0 10px 0;
    }
  }
`

function DetailsForm ({ question, response, handleRegisterResponse, saveResponse }) {
  const [disableForm, setDisableForm] = useState(true)
  const [view, setView] = useState('form')

  const { color, type, title, questions } = question

  useEffect(() => {
    if (response.length) {
      setDisableForm(false)

      response.forEach(res => {
        !res.response && setDisableForm(true)
      })
    }
  }, [response])

  return (
    <DetailsFormStyled onSubmit={event => { event.preventDefault(); saveResponse(setView) }}>
      {
        view === 'form' && (
          <>
            <HeaderDetails color={color} type={type} title={title} />
            {
              questions.length &&
                questions.map(q => {
                  return (
                    <div key={q.id} className='card-question'>
                      <Paragraph label={q.question} />
                      {q.input === 'selection' && <SelectionEmotion dataId={q.id} handleRegister={handleRegisterResponse} />}
                      {q.input === 'textarea' && <ResponseTextarea dataId={q.id} handleRegister={handleRegisterResponse} />}
                    </div>
                  )
                })
            }
            <BtnLinearGradient label='Enviar' disabled={disableForm} />
          </>
        )
      }
      {
        view === 'success' && <MsjSuccess label='Genial hemos recibido tu valioso comentario <br/>Juntos seremos mejores.!' target='/' />
      }
    </DetailsFormStyled>
  )
}

export default DetailsForm
