import { useState } from 'react'
import styled from 'styled-components'
import MeResponse from '../molecules/dashboard/me-response'
import TabContainer from '../molecules/dashboard/tab-container'

const ContainerResponseQuestionStyled = styled.div`

`

function ContainerResponseQuestion () {
  const optionTab = [
    {
      id: 'meRes',
      label: 'Mis respuestas',
      admin: false
    }
  ]

  const [selected, setSelected] = useState('meRes')
  const [view, setView] = useState('meRes')

  const handleSelectedOption = (value) => {
    setSelected(value)
    setView(value)
  }

  return (
    <ContainerResponseQuestionStyled>
      <TabContainer options={optionTab} handleSelectedOption={handleSelectedOption} selected={selected} />
      {view === 'meRes' && <MeResponse />}
      {view === 'globalRes' && <h1>Respuestas globales</h1>}
    </ContainerResponseQuestionStyled>
  )
}

export default ContainerResponseQuestion
