import styled from 'styled-components'

const IconAnswerEmotionStyled = styled.img`
  opacity: .5;
  cursor: pointer;
  transition: .3s all;

  &.Selected {
    opacity: 1;
  }

  &:hover{
    transform: scale(1.1);
  }
`
const initState = { width: 40, height: 40 }
function IconAnswerEmotion ({ img, size = initState, state = '', handleClick = () => {}, value }) {
  return (
    <IconAnswerEmotionStyled src={img} alt='Icon emotion' width={size.width} height={size.height} className={state} onClick={() => handleClick(value)} />
  )
}

export default IconAnswerEmotion
