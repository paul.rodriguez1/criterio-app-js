import Logo from '../../../img/logo-pragma.svg'

const LogoPragma = ({ size }) => {
  // const {size} = prop
  return <img src={Logo} alt='Logo Pragma' width={size.width} height={size.height} />
}

export default LogoPragma
