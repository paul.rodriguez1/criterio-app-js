import styled from 'styled-components'
import Logo from '../../../img/logo.png'

const ImgLogoStyled = styled.img`
  cursor: pointer;
  transition: .3s all;

  &:hover{
    transform: scale(1.1);
  }
`

export const LogoApp = ({ size }) => <ImgLogoStyled className='logoApp' src={Logo} alt='Logo principal de CriterioAPP' width={size.width} height={size.height} />
