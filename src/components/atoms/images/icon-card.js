import styled from 'styled-components'

const IconCardStyled = styled.img`
  transition: .3s all;
  &.large{
    height: 50px;
    width: 50px;
  }
  &.small{
    height: 43px;
    width: 43px;
  }
`

function IconCard ({ image, size = 'large' }) {
  return (
    <IconCardStyled src={image} alt='Imagen de card' width='50' height='50' className={size} />
  )
}

export default IconCard
