import { getImages } from '../../../utils/get-images'

function ImageSvg ({ image = 'success-form' }) {
  return (
    <img src={getImages(image)} alt='Imagen SVG' width='250' height='174' />
  )
}

export default ImageSvg
