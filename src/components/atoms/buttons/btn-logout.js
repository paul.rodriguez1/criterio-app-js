import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import { logout } from '../../../services/request'
import { handleModal } from '../../../redux/actions/utils'

const BtnLinearGradientStyled = styled.button`
  max-width: 300px;
  height: 30px;
  color: white;
  background-image: var(--LinearGradienteMain);
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 12px;
  font-weight: bold;
  text-decoration: none;
  opacity: 1;
  transition: .3s all;
  width: 100%;
  outline: none;
  cursor: pointer;
  border: none;

  :hover {
    opacity: .9;
    transform: scale(1.1);
  }

  &:disabled {
    cursor: no-drop;
    opacity: .2;
  }
`

function BtnLogout ({ label, disabled }) {
  const dispatch = useDispatch()
  const handleClick = () => {
    logout()
    dispatch(handleModal({ open: false }))
  }

  return (
    <BtnLinearGradientStyled disabled={disabled} onClick={handleClick}>
      {label}
    </BtnLinearGradientStyled>
  )
}

export default BtnLogout
