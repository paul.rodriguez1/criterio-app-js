import styled from 'styled-components'

const BtnSolidStyled = styled.button`
  width: 90px;
  height: 30px;
  color: white;
  background-color: ${({ color }) => color};
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 12px;
  font-weight: bold;
  text-decoration: none;
  opacity: 1;
  transition: .3s all;
  border: none;
  outline: none;
  cursor: pointer;

  :hover {
    opacity: .9;
    transform: scale(.9);
  }
`

function BtnSolid ({ label, color, handleClick = () => {} }) {
  return (
    <BtnSolidStyled color={color} onClick={() => { handleClick() }}>
      {label}
    </BtnSolidStyled>
  )
}

export default BtnSolid
