import { useDispatch } from 'react-redux'
import styled from 'styled-components'

import CloseModalImg from '../../../img/close-modal.png'
import { handleModal } from '../../../redux/actions/utils'

const BtnCloseModalStyled = styled.img`
  position: absolute;
  right: 5px;
  top: 10px;
  cursor: pointer;
  transition: .3s all;

  &:hover {
    transform: scale(1.1);
  }
`

function BtnCloseModal () {
  const dispatch = useDispatch()

  const CloseModal = () => {
    dispatch(handleModal({ open: false }))
  }

  return (
    <BtnCloseModalStyled src={CloseModalImg} alt='Close modal img' onClick={CloseModal} />
  )
}

export default BtnCloseModal
