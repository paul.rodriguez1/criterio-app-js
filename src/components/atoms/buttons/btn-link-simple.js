import { useDispatch } from 'react-redux'
import styled from 'styled-components'
import { handleModal } from '../../../redux/actions/utils'

const BtnLinkSimpleStyled = styled.div`
  display: flex;
  justify-content: flex-end;

  span{
    cursor: pointer;
    transition: .3s all;
    
    &:hover{
      font-weight: bold;
    }
  }
`

function BtnLinkSimple ({ label, target }) {
  const dispatch = useDispatch()

  const handleClick = () => {
    dispatch(handleModal({ open: true, action: target }))
  }
  return (
    <BtnLinkSimpleStyled>
      <span onClick={handleClick}>{label}</span>
    </BtnLinkSimpleStyled>
  )
}

export default BtnLinkSimple
