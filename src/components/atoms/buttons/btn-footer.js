import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components'
import { handleModal } from '../../../redux/actions/utils'

import ToolTips from '../ui/tooltips'

const BtnIconStyled = styled.img`
  width: 30px;
  height: 30px;
  transition: .3s all;
  cursor: pointer;

  &:hover {
    transform: scale(1.1);
  }
`

const BtnIcon = ({ target = false, img, dataTip, position }) => {
  const history = useHistory()

  const { login: { auth } } = useSelector(state => state)
  const dispatch = useDispatch()

  const handleClick = () => {
    auth
      ? history.push(target)
      : dispatch(handleModal({ open: true, action: 'login', tmpUrl: target }))
  }

  return (
    <ToolTips dataTip={dataTip} place={position}>
      <BtnIconStyled onClick={handleClick} alt='Btn de menu' src={img} />
    </ToolTips>
  )
}

export default BtnIcon
