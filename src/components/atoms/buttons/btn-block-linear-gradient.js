import styled from 'styled-components'

const BtnLinearGradientStyled = styled.button`
  width: 90px;
  height: 30px;
  color: white;
  background-image: var(--LinearGradienteMain);
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 12px;
  font-weight: bold;
  text-decoration: none;
  opacity: 1;
  transition: .3s all;
  width: 100%;
  max-width: 300px;
  outline: none;
  cursor: pointer;
  border: none;

  :hover {
    opacity: .9;
    transform: scale(1.1);
  }

  &:disabled {
    cursor: no-drop;
    opacity: .2;
  }
`

function BtnLinearGradient ({ label, disabled, handleClick = false }) {
  const handleOnClick = e => {
    handleClick && e.preventDefault()
    handleClick && handleClick()
  }
  return (
    <BtnLinearGradientStyled disabled={disabled} onClick={handleOnClick}>
      {label}
    </BtnLinearGradientStyled>
  )
}

export default BtnLinearGradient
