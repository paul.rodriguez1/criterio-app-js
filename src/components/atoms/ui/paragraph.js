import styled from 'styled-components'

const ParagraphStyled = styled.p`
  text-align: justify;
  max-width: 600px;
  margin: 20px auto;

  &.msj-success {
    font-size: 23px;
    font-weight: normal;
    text-align: center;
  }
`

function Paragraph ({ label, classElement = '' }) {
  return (
    <ParagraphStyled className={classElement} dangerouslySetInnerHTML={{ __html: label }} />
  )
}

export default Paragraph
