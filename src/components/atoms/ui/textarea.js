import styled from 'styled-components'

const TextAreaElementStyled = styled.textarea`
  background-color: var(--ColorInput);
  width: 100%;
  border-radius: 10px;
  box-shadow: var(--ShadowInput);
  border: none;
  outline: none;
  padding: 15px;
  font-family: var(--FontFamily);
`

const TextAreaElement = ({ value, onChange, handleOnBlur }) => <TextAreaElementStyled rows={4} value={value} onChange={onChange} onBlur={handleOnBlur} />

export default TextAreaElement
