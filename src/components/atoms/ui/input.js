import styled from 'styled-components'

const InputElementStyled = styled.input`
  background-color: var(--ColorInput);
  width: 100%;
  border-radius: 10px;
  box-shadow: var(--ShadowInput);
  border: 1px solid transparent;
  outline: none;
  padding: 10px 15px;
  font-family: var(--FontFamily);
  transition: .3s all;

  &.error {
    border-color: var(--ColorError);
    border-left: 20px solid var(--ColorError);
    padding: 10px 36px 10px 15px;
  }
`

const InputElement = ({ type = 'text', config, error }) => {
  return (
    <InputElementStyled className={error && 'error'} type={type} {...config} />
  )
}

export default InputElement
