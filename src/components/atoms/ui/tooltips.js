import styled from 'styled-components'
import ReactTooltip from 'react-tooltip'

const TootltipStyled = styled.a`
  height: 100%;
  cursor: pointer;
  display: flex;
  align-items: center;
`

export default function ToolTips ({ children, type = 'dark', place = 'top', dataTip, effect = 'solid' }) {
  return (
    <TootltipStyled data-tip={dataTip}>
      {children}
      <ReactTooltip place={place} type={type} effect={effect} />
    </TootltipStyled>
  )
}
