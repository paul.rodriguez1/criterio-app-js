import styled from 'styled-components'

const ContainerCirculeStyled = styled.div`
  border: 3px solid ${({ color }) => color};
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  
  &.large {
    width: 90px;
    min-width: 90px;
    height: 90px;
  }
  
  &.small {
    width: 70px;
    min-width: 70px;
    height: 70px;
  }
`

function ContainerCircule ({ children, color, size = 'large' }) {
  return (
    <ContainerCirculeStyled color={color} className={size}>
      {children}
    </ContainerCirculeStyled>
  )
}

export default ContainerCircule
