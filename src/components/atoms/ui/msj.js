import { useEffect, useState } from 'react'
import styled from 'styled-components'
import { getMessageText } from '../../../utils/get-message'
import Loading from './spinner'

const MsjModalStyled = styled.div`
  position: absolute;
  left: 15px;
  right: 15px;
  bottom: 15px;
  font-weight: 400;
  text-align: center;
  
  &.error {
    color: red;
  }

  &.success {
    color: green;
  }
`

function MsjModal ({ config }) {
  const [text, setText] = useState('')

  useEffect(() => {
    const { type, msj } = config
    let delay = 2000
    msj && setText(msj)
    delay = type === 'error' && 5000
    delay && setTimeout(() => {
      setText('')
    }, delay)
  }, [config])

  const Text = config.type === 'loading'
    ? <Loading color='#4A4A4A' size='20px' label={getMessageText(text)} type='ClipLoader' styles='center-row' />
    : text
  return <MsjModalStyled className={config.type}>{Text}</MsjModalStyled>
}

export default MsjModal
