import styled from 'styled-components'

import ClipLoader from 'react-spinners/ClipLoader'
import HashLoader from 'react-spinners/HashLoader'

const SpinnerStyled = styled.div`
  display: flex;
  gap: 10px;
  align-items: center;

  &.center-row {
    justify-content: center;
  }

  &.center-column {
    justify-content: center;
    flex-direction: column;
    gap:20px
  }

  &.font-bigger {
    font-size: 20px;
  }
`

function Spinner ({ type, color, label, styles }) {
  return (
    <SpinnerStyled className={styles}>
      {type === 'ClipLoader' && <ClipLoader color={color} />}
      {type === 'HashLoader' && <HashLoader color={color} />}
      {label}
    </SpinnerStyled>
  )
}

export default Spinner
