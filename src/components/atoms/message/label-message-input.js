import styled from 'styled-components'
import { fadeInMessageAnimation } from '../../../styles/animation'
import { getIcon } from '../../../utils/get-icons'
import ToolTips from '../ui/tooltips'

const LabelMessageInputStyled = styled.span`
  color: var(--ColorError);
  font-size: .85em;
  position: absolute;
  bottom: 3.5px;
  right: 8px;
  &.fade-in {
    ${fadeInMessageAnimation()}
  } 
  &.error a { 
    color: var(--ColorError);

  } 
`

function LabelMessageInput ({ error = {} }) {
  const { message } = error
  return (
    <>
      {
      error.message && (
        <LabelMessageInputStyled className={error ? 'error fade-in' : ''}>
          <ToolTips dataTip={message} place='left'>
            {getIcon('input-warning')}
          </ToolTips>
        </LabelMessageInputStyled>
      )
    }
    </>
  )
}

export default LabelMessageInput
