import styled from 'styled-components'
import { getImages } from '../../../utils/get-images'
import ImageSvg from '../images/img-svg'

const NoDataStyled = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`

function NoData () {
  return (
    <NoDataStyled>
      <ImageSvg image='not-found' />
      <h1>No data</h1>
    </NoDataStyled>
  )
}

export default NoData
