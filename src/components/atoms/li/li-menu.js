import { Link } from 'react-router-dom'

import styled from 'styled-components'
import { getIcon } from '../../../utils/get-icons'

const OptionDashboardStyled = styled(Link)`
  border: 1px solid var(--ColorBorder);
  padding: 10px 10px 10px 20px;
  display: flex;
  border-radius: 10px;
  box-shadow: var(--ShadowBox);
  min-height: 60px;
  align-items: center;
  gap: 10px;

  &, svg, span {
    transition: .3s all;
  }
  
  &:hover {
    font-weight: bold;
    transform: scale(1.1);
    border-color: ${({ color }) => color};

    svg {
      transform: scale(1.1);
    }

    span {
      font-size: 1.05em;
    }
  }
`

function OptionDashboard ({ target, color, label, icon }) {
  return (
    <OptionDashboardStyled to={target} color={color}>
      {getIcon(icon)}
      <span>{label}</span>
    </OptionDashboardStyled>
  )
}

export default OptionDashboard
