import styled from 'styled-components'

const TabItemStyled = styled.li`

`

function TabItem ({ label, selected, handleSelectedOption, id }) {
  return (
    <TabItemStyled className={selected === id && 'active'} onClick={() => handleSelectedOption(id)}>
      {label}
    </TabItemStyled>
  )
}

export default TabItem
