import styled from 'styled-components'

const TitleCardStyled = styled.h3`
  margin: 0;
  font-weight: bold;

  &.large {
    font-size: 24px;
  }
`

function TitleCard ({ label, size }) {
  return (
    <TitleCardStyled className={size}>
      {label}
    </TitleCardStyled>
  )
}

export default TitleCard
