import Styled from 'styled-components'

// Components
import LogoPragma from '../images/logo-pragma'

const TitleModalStyled = Styled.h1`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 20px;
  font-weight: normal;
  flex-direction: column;
  gap: 10px;
`

function TitleModal ({ label }) {
  const SizeLogo = {
    width: 100,
    height: 28
  }

  return (
    <TitleModalStyled>
      <LogoPragma size={SizeLogo} />
      {label}
    </TitleModalStyled>
  )
}

export default TitleModal
