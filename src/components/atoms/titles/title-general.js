import Styled from 'styled-components'

// Components
import LogoPragma from '../images/logo-pragma'

const TitlesGeneralStyled = Styled.h1`
  display: flex;
  align-items: center;
  justify-content: center;

  &.in-line {
    img {
      margin-top: 15px;
    }
  }
  
`

function TitlesGeneral ({ label, classesComponente }) {
  const SizeLogo = {
    width: 115,
    height: 28
  }

  return (
    <TitlesGeneralStyled className={classesComponente}>
      {label}
      <LogoPragma size={SizeLogo} />
    </TitlesGeneralStyled>
  )
}

export default TitlesGeneral
