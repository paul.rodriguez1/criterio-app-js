import styled from 'styled-components'

const TitleCardStyled = styled.h1`
  margin: 0;
  font-weight: bold;
  text-align: center;
  display: flex;
  flex-direction: column;
  font-size: 25px;

  span {
    font-size: 20px;
  }
`

function TitleCard ({ label }) {
  return (
    <TitleCardStyled>
      <span>Hola</span> {label}
    </TitleCardStyled>
  )
}

export default TitleCard
