import styled from 'styled-components'

// Components
import TitleCard from '../atoms/titles/title-user'
import Layout from '../organisms/Layout'
import Paragraph from '../atoms/ui/paragraph'
import BtnLinearGradient from '../atoms/buttons/btn-block-linear-gradient'
import { useHistory } from 'react-router-dom'
import ImageSvg from '../atoms/images/img-svg'

const NotFoundStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 20px;

  section {
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 5px;
  }
`

export default function NotFound () {
  const history = useHistory()
  const handleClick = () => {
    history.push('/')
  }
  return (
    <Layout>
      <NotFoundStyled>
        <TitleCard label='Algo no esta bien.' classesComponente='in-line' />
        <Paragraph label='La pagina que estas buscando no existe, pero no te preocupes volvamos a iniciar.' />
        <ImageSvg image='not-found' />
        <BtnLinearGradient label='Volvamos a iniciar' handleClick={handleClick} />
      </NotFoundStyled>
    </Layout>
  )
}
