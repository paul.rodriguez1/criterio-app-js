import { useEffect, useState } from 'react'
import styled from 'styled-components'

// Components
import TitleCard from '../atoms/titles/title-user'
import Layout from '../organisms/Layout'
import ListCard from '../organisms/list-card'

const ConfigQuestionStyled = styled.div`

`

function ConfigQuestion () {
  const [data, setData] = useState([])

  useEffect(() => {

  }, [])

  return (
    <Layout>
      <ConfigQuestionStyled>
        <TitleCard label='Configuración' classesComponente='in-line' />
        <ListCard data={data} />
      </ConfigQuestionStyled>
    </Layout>
  )
}

export default ConfigQuestion
