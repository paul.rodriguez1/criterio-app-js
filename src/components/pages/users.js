import { useEffect, useState } from 'react'
import styled from 'styled-components'

// Components
import TitleCard from '../atoms/titles/title-user'
import BtnLogout from '../atoms/buttons/btn-logout'
import Layout from '../organisms/Layout'
import { useSelector } from 'react-redux'

const UserPageStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 20px;

  section {
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 5px;
  }
`

function UserPage () {
  const [user, setUser] = useState({})

  const { login: { userLoged } } = useSelector(state => state)

  useEffect(() => {
    setUser(userLoged)
  }, [userLoged])

  return (
    <Layout>
      <UserPageStyled>
        <TitleCard label={user.name} classesComponente='in-line' />
        <section>
          <strong>Email:</strong> {user.email}
          {user.admin && <span>Administrador</span>}
        </section>
        <BtnLogout label='Cerrar sesión' />
      </UserPageStyled>
    </Layout>
  )
}

export default UserPage
