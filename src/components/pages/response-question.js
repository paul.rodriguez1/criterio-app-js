import Layout from '../organisms/Layout'
import styled from 'styled-components'
import ContainerResponseQuestion from '../organisms/response-question-container'

const ResponseQuestionStyled = styled.div`

`

function ResponseQuestion () {
  return (
    <ResponseQuestionStyled>
      <Layout>
        <ResponseQuestionStyled>
          <ContainerResponseQuestion />
        </ResponseQuestionStyled>
      </Layout>
    </ResponseQuestionStyled>
  )
}

export default ResponseQuestion
