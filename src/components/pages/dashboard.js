import { useEffect, useState } from 'react'
import styled from 'styled-components'

// Components
import TitleCard from '../atoms/titles/title-user'
import Layout from '../organisms/Layout'
import { useSelector } from 'react-redux'
import MenuDashboard from '../organisms/menu-option'

const DashBoardStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  gap: 20px;

  section {
    display: flex;
    align-items: center;
    flex-direction: column;
    gap: 5px;
  }
`

function DashBoard () {
  const [user, setUser] = useState({})

  const { login: { userLoged } } = useSelector(state => state)

  useEffect(() => {
    setUser(userLoged)
  }, [userLoged])

  return (
    <Layout>
      <DashBoardStyled>
        <TitleCard label={user.name} classesComponente='in-line' />
        <MenuDashboard />
      </DashBoardStyled>
    </Layout>
  )
}

export default DashBoard
