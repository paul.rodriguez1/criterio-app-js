// Components
import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { getQuestions } from '../../services/request'
import { handleModal } from '../../redux/actions/utils'
import TitlesGeneral from '../atoms/titles/title-general'
import Paragraph from '../atoms/ui/paragraph'
import Layout from '../organisms/Layout'
import ListCard from '../organisms/list-card'

function Home () {
  const [data, setData] = useState([])
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(handleModal({ open: true, action: 'loading', label: 'Cargando encuentas' }))
    getQuestions(setData)
    setTimeout(() => {
      dispatch(handleModal({ open: false, action: '', label: '' }))
    }, 1000)
  }, [])
  return (
    <Layout>
      <>
        <TitlesGeneral label='Bienvenido a ' classesComponente='in-line' />
        <Paragraph label='En pragma estamos seguros que debemos mejorar dia a dia, y por eso nos apoyamos en nuestro valor mas preciado, nuestros clientes haznos saber que opinas de nosotros para crecer juntos.' />
        <ListCard data={data} />
      </>
    </Layout>
  )
}

export default Home
