import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router'
import { getQuestion, saveResponseQuestion } from '../../services/request'
import { handleModal } from '../../redux/actions/utils'

// import components
import DetailsForm from '../organisms/form-details'
import Layout from '../organisms/Layout'
import { getMessageText } from '../../utils/get-message'
import NoData from '../atoms/message/no-data'

export default function Details () {
  const stateInitial = {
    color: '',
    title: '',
    type: '',
    questions: []
  }
  const [question, setQuestion] = useState(stateInitial)
  const [response, setResponse] = useState([])

  const { id } = useParams()
  const dispatch = useDispatch()
  const { login: { auth } } = useSelector(state => state)

  useEffect(async () => {
    dispatch(handleModal({ open: true, action: 'loading', label: getMessageText('loadingInfo') }))
    const data = await getQuestion(id)
    setQuestion(data)
    setResponse(data.questions)
    setTimeout(() => {
      dispatch(handleModal({ open: false, action: '', label: '' }))
    }, 1000)
  }, [])

  const handleRegisterResponse = ({ id, value }) => {
    if (response.length) {
      const newResponse = response.map(r => {
        return id.toString() === r.id.toString() ? { ...r, response: value } : r
      })
      setResponse(newResponse)
    }
  }

  const saveResponse = async setView => {
    try {
      dispatch(handleModal({ open: true, action: 'loading', label: getMessageText('registerInfo') }))
      const result = await saveResponseQuestion(id, response, auth, question.title)
      setTimeout(() => {
        result && setView('success')
        dispatch(handleModal({ open: false, action: '', label: '' }))
      }, 1000)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Layout>
      <>
        {
          id
            ? <DetailsForm question={question} response={response} handleRegisterResponse={handleRegisterResponse} saveResponse={saveResponse} />
            : <NoData />
        }
      </>
    </Layout>
  )
}
