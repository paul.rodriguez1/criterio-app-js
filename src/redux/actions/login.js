import { USER_LOGED, STATE_AUTH } from '../const/actionsType'

export const setUserLoged = payload => ({
  type: USER_LOGED,
  payload
})

export const setAuth = payload => ({
  type: STATE_AUTH,
  payload
})
