import { HANDLE_MODAL } from '../const/actionsType'

export const handleModal = payload => ({
  type: HANDLE_MODAL,
  payload
})
