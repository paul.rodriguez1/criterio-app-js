import { GET_QUESTIONS_START, GET_QUESTION_START } from '../const/actionsType'

export const getQuestions = payload => ({
  type: GET_QUESTIONS_START,
  payload
})

export const getQuestion = payload => ({
  type: GET_QUESTION_START,
  payload
})
