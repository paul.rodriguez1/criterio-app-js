import { combineReducers } from 'redux'
import login from './login'
import utils from './utils'

const rootReducer = combineReducers({
  login,
  utils
})

export default rootReducer
