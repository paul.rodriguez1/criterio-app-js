import { STATE_AUTH, USER_LOGED } from '../const/actionsType'

const initialState = { auth: '', userLoged: {} }

export default function reducer (state = initialState, action) {
  const { type, payload } = action
  switch (type) {
    // set auth
    case STATE_AUTH:
      return { ...state, auth: payload }

      // set auth
    case USER_LOGED:
      return { ...state, userLoged: payload }

    default:
      return { ...state }
  }
}

// cognito y amplify
