import { HANDLE_MODAL } from '../const/actionsType'

const initialState = { modal: { open: false, action: '', urltmp: '' } }

export default function reducer (state = initialState, action) {
  const { type, payload } = action
  const { modal: initModal } = initialState
  switch (type) {
    case HANDLE_MODAL:

      return { ...state, modal: payload.open ? payload : initModal }

    default:
      return { ...state }
  }
}
