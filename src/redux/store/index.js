import { createStore, compose } from 'redux'

import rootReducer from '../reducers'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const configureStore = () => {
  return createStore(rootReducer, composeEnhancers())
}
export default configureStore
