import { useEffect } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { GlobalStyles } from './styles/global'
import { authFb } from './firebase'
// actions
import { setAuth, setUserLoged } from './redux/actions/login'

// Hooks
import { getUser } from './services/request'

// Componentes
import Home from './components/pages/home'
import Details from './components/pages/details'
import User from './components/pages/users'
import PrivateRoute from './hooks/privateRoute'
import DashBoard from './components/pages/dashboard'
import NotFound from './components/pages/noPages'
import Modal from './components/organisms/modal-container'
import ResponseQuestion from './components/pages/response-question'
import ConfigQuestion from './components/pages/config-question'

function App () {
  const dispatch = useDispatch()
  const storeState = useSelector((state) => state)
  const { utils: { modal } } = storeState

  useEffect(() => {
    authFb.onAuthStateChanged(async (user) => {
      const uid = user ? user.uid : ''
      const userLoged = await getUser(uid)
      dispatch(setAuth(uid))
      dispatch(setUserLoged(userLoged))
      console.log(user ? 'connected' : 'not connect')
    })
  }, [])

  return (
    <>
      <GlobalStyles />
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={Home} />
          <PrivateRoute exact path='/encuesta/:id'>
            <Details />
          </PrivateRoute>
          <PrivateRoute exact path='/user'>
            <User />
          </PrivateRoute>
          <PrivateRoute exact path='/dashboard'>
            <DashBoard />
          </PrivateRoute>
          <PrivateRoute exact path='/response-question'>
            <ResponseQuestion />
          </PrivateRoute>
          <PrivateRoute exact path='/config-question'>
            <ConfigQuestion />
          </PrivateRoute>
          <Route component={NotFound} />
        </Switch>
        <Modal {...modal} />
      </BrowserRouter>
    </>
  )
}

export default App
