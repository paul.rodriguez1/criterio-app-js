import { db, authFb } from '../firebase'
import { getMessageText } from '../utils/get-message'

export const getQuestions = (setData) => {
  db.collection('questions').orderBy('create', 'asc').onSnapshot((querySnapshot) => {
    const data = []
    querySnapshot.forEach(doc => {
      data.push({ ...doc.data(), id: doc.id })
    })
    setData(data)
  })
}

export const getQuestion = async (id) => {
  const doc = await db.collection('questions').doc(id).get()
  return doc ? { ...doc.data(), id: doc.id } : {}
}

export const saveResponseQuestion = async (id, response, uid, title) => {
  const docRef = await db.collection('response').add({
    idQuestion: id, response, uid, date: Date.now(), title
  })
  return docRef.id
}

export const getUser = async (uid) => {
  let userLoged
  const querySnapshot = await db.collection('users').where('uid', '==', uid).get()
  querySnapshot.forEach((doc) => {
    userLoged = doc.data()
  })

  return userLoged
}

export const signUpServices = async ({ email = '', password = '', nombre = '' }) => {
  try {
    const signup = await authFb.createUserWithEmailAndPassword(email, password)
    const { user: { uid } } = signup
    let code
    if (uid) {
      await db.collection('users').add({
        email, name: nombre, uid, admin: false
      })
      code = 'registerSuccess'
    } else {
      code = 'errorGeneral'
    }
    return { type: 'success', msj: getMessageText(code) }
  } catch (error) {
    const { code } = error
    return { type: 'error', msj: getMessageText(code) }
  }
}
export const signInServices = async ({ email = '', password = '' }) => {
  if (email !== '' && password !== '') {
    try {
      await authFb.signInWithEmailAndPassword(email, password)
      return { type: 'success', msj: getMessageText('loginSuccess') }
    } catch (error) {
      const { code } = error
      return { type: 'error', msj: getMessageText(code) || error }
    }
  } else {
    return { type: 'error', msj: getMessageText(credentialRequired) }
  }
}

export const logout = async () => {
  await authFb.signOut().then()
}

export const getOptionDashboard = async () => {
  const options = []
  const querySnapshot = await db.collection('optionDashboard')
    .orderBy('order', 'asc').get()

  querySnapshot.forEach(doc => {
    options.push({ ...doc.data(), id: doc.id })
  })

  return options
}

export const getResponses = async (uid) => {
  const responses = []
  const querySnapshot = await db.collection('response').where('uid', '==', uid).get()

  querySnapshot.forEach(doc => {
    responses.push({ ...doc.data(), id: doc.id })
  })

  return responses
}
