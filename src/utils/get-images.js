import ServicioAlCliente from '../img/card/servicio-al-cliente.png'
import ProcesosAdministrativos from '../img/card/procesos-administrativos.png'
import SatisfaccionProducto from '../img/card/satisfaccion-producto.png'
import ImageDefault from '../img/card/defult-images.svg'
import SuccessForm from '../img/success-form.png'
import NotFound from '../img/notFound.svg'
import Angry from '../img/card/emotion-answer/1-angry.png'
import Bored from '../img/card/emotion-answer/2-bored.png'
import Happy from '../img/card/emotion-answer/3-happy.png'
import InLove from '../img/card/emotion-answer/4-in-love.png'

export const getImages = (img) => {
  const LIST_IMAGES = {
    'servicio-cliente': ServicioAlCliente,
    'procesos-administrativos': ProcesosAdministrativos,
    'satisfaccion-producto': SatisfaccionProducto,
    'success-form': SuccessForm,
    'not-found': NotFound,
    Angry: Angry,
    Bored: Bored,
    Happy: Happy,
    InLove: InLove
  }

  const DEFAULT_IMAGE = ImageDefault

  return LIST_IMAGES[img] || DEFAULT_IMAGE
}
