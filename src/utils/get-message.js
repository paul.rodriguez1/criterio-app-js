export const getMessageText = (code) => {
  const LIST_CODE = {
    // Generales
    loadingInfo: 'Cargando información',
    validandoInfo: 'Validando información',
    registerInfo: 'Registrando sus respuestas',
    // Login
    pwdNotMatch: 'Las contraseñas ingresadas no coinciden',
    registerSuccess: 'El usuario registrado correctamente, Bienvenido...',
    loginSuccess: 'Bienvenido...',
    errorGeneral: 'Error general, comuniquese con el administrador del sistema.',
    credentialRequired: 'Las credenciales son requeridas',
    'auth/email-already-in-use': 'El usuario ya esta registrado.',
    'auth/weak-password': 'La contraseña debe contener minimo 6 cacteres.',
    'auth/wrong-password': 'Credenciales incorrectas',
    'auth/user-not-found': 'Credenciales incorrectas'
  }

  return LIST_CODE[code] || ''
}
