import { AiOutlineSchedule } from 'react-icons/ai'
import { GrDocumentConfig } from 'react-icons/gr'
import { HiUserGroup } from 'react-icons/hi'
import { RiErrorWarningLine } from 'react-icons/ri'

export const getIcon = (icon, size = '25px') => {
  let Icon = ''

  const LIST_ICON = {
    'dashboard-list-requests': AiOutlineSchedule,
    'dashboard-config-question': GrDocumentConfig,
    'dashboard-admin-users': HiUserGroup,
    'input-warning': RiErrorWarningLine
  }
  Icon = LIST_ICON[icon]
  return <Icon size={size} />
}
